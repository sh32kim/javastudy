public class MergeSort {

    public static void mergeSort(int[] nums) {
        sort(nums, 0, nums.length);
    }
    public static void sort(int[] nums, int low, int high) {
        if (high - low <= 1) {
            return;
        }
        int mid = (low+high) / 2;
        sort(nums, low, mid);
        sort(nums, mid, high);
        merge(nums, low, mid, high);
    }
    public static void merge(int[] nums, int low, int mid, int high) {
        int[] temp = new int[high-low];
        int t = 0; int l = low; int r = mid;

        while (l < mid && r < high) {
            if (nums[l] < nums[r]) {
                temp[t++] = nums[l++];
            } else {
                temp[t++] = nums[r++];
            }
        }

        while (l < mid) {
            temp[t++] = nums[l++];
        }

        while (r < high) {
            temp[t++] = nums[r++];
        }

        for (int i=low; i<high; i++) {
            nums[i] = temp[i-low];
        }
    }

    public static void main(String[] args) {
        int[] test1 = {11, 3, 28, 43, 9, 4};
        int[] test2 = {1, 2, 3, 4, 5};
        int[] test3 = {5, 4, 3, 2, 1};
        int[] test4 = {3, 3, 1, 1, 2};
        int[] test5 = {4, 2};
        MergeSort sorting = new MergeSort();
        MergeSort.mergeSort(test1);
        sorting.printFunction(test1);
    }

    public void printFunction(int[] nums) {
        for (int i=0; i<nums.length; i++) {
            System.out.print(nums[i] + " ");
        }
        System.out.println();
    }

}